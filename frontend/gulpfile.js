"use strict";

/**
 * Requirements
 */
var gulp = require('gulp');
var argv = require('yargs').argv;
var exec = require('child_process').exec;
var _ = require('lodash');
var fs = require("fs");
var versionFile = './version';
var srcpath = './src/';
var modpath = srcpath + 'modules/';

/**
 * default config
 */
var config = {
    // default environment
    env: "dev",
    version: fs.existsSync(versionFile) ? fs.readFileSync(versionFile, "utf-8") : '0.0.0'
};

/**
 * Overwrite default config
 * @param argv
 */
config.parseArgv = function (argv) {
    if (argv.env) {
        config.env = argv.env;
    }
    if (argv.version) {
        config.version = argv.version;
    }
};

/**
 * Overwrite default values with parameters
 */
config.parseArgv(argv);

/**
 * Small help
 */

gulp.task('default', function () {
    console.log('______________________________________________________________________________________________________\n');
    console.log('Gulp Commands');
    console.log('______________________________________________________________________________________________________\n');
    console.log('Avaliable Commands:');
    console.log('create-module - creates a module following a standard module organization');
    console.log('relase - releases a versioned packed application to be run on production');
    console.log('deploy - deploys the application locally as if it was on the production or developmnent environment');
    console.log('mocked-server - run a mock server based on the swagger API defined in http://localhost:8888');
    console.log('mocked-tests - run the tests created for the application against a mocked server');
    console.log('test - run the tests created for the application against a real backend defined in src/resources/config.js');
    console.log('______________________________________________________________________________________________________\n');
    console.log('Options:');
    console.log('--env          [dev|prod] informs the environment tasks are running. Default: dev');
    console.log('--releasetype  [major,minor,patch] type of release for automated versioning');
    console.log('--version      [0.0.0] semantic versioning string for release');
    console.log('--modname      [string] - lower case string (dashes allowed) name of the module');
    // console.log('--branch       [branch] branch on which the command must be run from (default is current branch)');
    // console.log('--commit       [sha]  get files from specified commit from git');
    console.log('______________________________________________________________________________________________________\n');
    console.log('Examples:');
    console.log('\n#1: gulp deploy --env prod --gitpull 38ec2c5 --relase major --version 0.0.0');
    console.log('\nThis command would create a redistributable release versioned as 1.0.0 from commit 38ec2c5');
    console.log('\n#2: gulp create-module --modname=example_module_name');
    console.log('\nThis command would create a module named example_module_name in the src/modules/ folder');
    console.log('______________________________________________________________________________________________________\n');
    console.log('NOTE: you can only release from the master branch!');
    console.log('______________________________________________________________________________________________________\n');
    return false;
});

/**
 * Prepares the release on dist dir and packs it as
 */
gulp.task('release', function () {
    /**
     * Info about the task
     */
    console.info('Preparing for release...');

    /**
     * Releases are always made as if they were for production
     * @type {string}
     */
    config.env = 'prod';

    /**
     * Releases can only be created from the master branch
     */
    gulp.start('check-release-branch');

    /**
     * Will run tests, that will make the deploy for the desired environment
     */
    gulp.start('test');

    /**
     * If everything ran well, pack the dist file with current version
     */
    gulp.start('get-next-version');

    /**
     * Now we pack everything as a zip file
     */
    gulp.start('generate-dist-version-package');

    /**
     * Update versioning file
     */
    gulp.start('update-versioning-file');

    return false;
});

/**
 * Deploys the application as if it was running on a given environment (dev or prod)
 */
gulp.task('deploy', function () {
    /**
     * Info about the task
     */
    console.info('Preparing for deploy... Enviroment ' + config.env);

    if (config.env == 'dev') {
        var proc = exec('cd src && bower install && cd ..');
        proc.stdout.on('data', function (data) {
            console.log(data);
        });
    } else

        return false;
});

/**
 * Run all tests on the application as if it was running on a given environment (dev or prod)
 */
gulp.task('test', function () {
    /**
     * Info about the task
     */
    console.info('Preparing for tests... Enviroment ' + config.env);

    gulp.start('deploy');

    return false;
});

/**
 * Check if the current branch is master. Releases should only be created from the master branch
 */
gulp.task('check-release-branch', function () {
    /**
     * Info about the task
     */
    console.info('Checking if in branch... Enviroment ' + config.env);

    exec('git branch', function (error, stdout, stderr) {
        var branches = stdout.split('\n');
        var result = _.some(branches, function (branch) {
            return branch == '* master';
        });
        assertError(
            !result,
            'YOU MUST RELEASE FROM MASTER BRANCH ONLY'
        );
    });

    return false;
});

gulp.task('generate-dist-version-package', function () {
    /**
     * Info about the task
     */
    console.info('Preparing for deploy... Enviroment ' + config.env);

    return false;
});

gulp.task('get-next-version', function () {
    /**
     * Info about the last version created
     */
    console.info('Last version was: ' + config.version);

    /**
     * Calculates version based on parameters
     */
    var version = config.version.split('.');

    switch (argv.releasetype) {
        case 'major':
            version[0]++;
            version[1] = version[2] = 0;
            break;
        case 'minor':
            version[1]++;
            break;
        default:
            version[2]++;
            break;
    }

    /**
     * Next version calculated
     */
    config.version = version.join('.');

    console.info('Next version is: ' + config.version);

    return false;
});

gulp.task('update-versioning-file', function () {
    /**
     * Update versioning file with current number
     */
    console.info('Actual version is: ' + config.version);

    /**
     * Replace version file with new content
     */
    fs.existsSync(versionFile) ? fs.unlinkSync(versionFile) : null
    fs.writeFile(versionFile, config.version);

    return false;
});

/**
 * Create a module structure based on the module skeleton
 */
gulp.task('create-module', function () {
    /**
     * Info about the task
     */
    console.info('Preparing for Module creation...');

    /**
     * Modules can only be created on dev environment
     * @type {string}
     */
    config.env = 'dev';

    assertError(
        !argv.modname,
        'YOU MUST INFORM THE NEW MODULE NAME'
    );

    assertError(
        fs.existsSync(modpath + argv.modname),
        'THERE IS ALREADY A MODULE BY THAT NAME ON THE MODULES FOLDER'
    );

    var modname = argv.modname;

    /**
     * Create directories
     */
    modmkdir(modname);
    modmkdir(modname, 'components');
    modmkdir(modname, 'components/controllers');
    modmkdir(modname, 'components/directives');
    modmkdir(modname, 'components/models');
    modmkdir(modname, 'components/services');
    modmkdir(modname, 'components/services/clients');
    modmkdir(modname, 'components/services/providers');
    modmkdir(modname, 'components/services/utils');
    modmkdir(modname, 'components/templates');
    modmkdir(modname, 'components/templates/default/');
    modmkdir(modname, 'resources');
    modmkdir(modname, 'resources/css');
    modmkdir(modname, 'resources/images');
    modmkdir(modname, 'resources/translations');

    var menuIndex = registerModule(modname);
    createModuleJs(modname);
    createConfigJs(modname);
    createTransJs(modname);
    createMenuJs(modname, menuIndex);
    createRoutesJs(modname);
    createIndexJs(modname);
    createModuleCss(modname);


    return false;
});

/***********************************************************************************************************************
 *
 * AUXILIAR FUNCTIONS FOR THE TASKS
 *
 **********************************************************************************************************************/
/**
 * Deals with checks. In case they are true, process is killed and message is displayed
 */
function assertError(assertion, message) {
    if (assertion) {
        console.error("\n\n\n|||||||}}}}}}}>>>>>>>       " + message + "       <<<<<<<{{{{{{{|||||||\n\n\n");
        process.exit(1);
    }
}

/**
 * Creates module directory
 */
function modmkdir(modname, path) {
    var dirpath = path ? (modpath + modname + '/' + path) : (modpath + modname);
    exec('mkdir ' + dirpath, function (error, stdout, stderr) {
        if (error) {
            exec('rm -rf ' + modpath + modname);
            assertError(true, 'COuld not create module dir');
        }
        exec('touch ' + dirpath + '/.gitkeep');
    });
}

function registerModule(modname) {
    var systemjsGarbage = 'var modules = ';
    var modFilePath = './src/modules.js';
    var modContent = fs.readFileSync(modFilePath, 'utf8');
    var modules = JSON.parse(modContent.replace(systemjsGarbage, ''));
    var menuIndex = Object.keys(modules).length + 1;
    modules[modname] = '/modules/' + modname;
    fs.writeFileSync(modFilePath, systemjsGarbage + JSON.stringify(modules));
    return menuIndex;
}

/**
 * Creates module directory
 */
function createModuleCss(modname, path) {
    var dirpath = path ? (modpath + modname + '/' + path) : (modpath + modname);
    exec('touch ' + dirpath + '/resources/css/module.css');
}

function createModuleJs(modname) {
    var dirpath = modpath + modname;
    var filename = dirpath + '/module.js';

    exec('echo \'(function (angular, undefined) {\\n'
        + '    "use strict";\\n\\n'
        + '    angular.module("' + modname + '", []);\\n'
        + '    angular.module("app").requires.push("' + modname + '");\\n'
        + '})(angular);\' > ' + filename);
}

function createMenuJs(modname,menuIndex) {
    var dirpath = modpath + modname;
    var filename = dirpath + '/menu.js';

    exec("echo \"var menu = {'index': " + menuIndex + ", 'icon' : 'fa fa-folder', 'name' : '" +
        modname + "', 'state'  : '" + modname +
        "'}\" > " + filename);
}

function createTransJs(modname) {
    var dirpath = modpath + modname + '/resources/translations';
    var filename = dirpath + '/en.js';
    exec("echo \"var trans = [];\" > " + filename);
    var filename = dirpath + '/es.js';
    exec("echo \"var trans = [];\" > " + filename);
    var filename = dirpath + '/pt.js';
    exec("echo \"var trans = [];\" > " + filename);
    var filename = dirpath + '/fr.js';
    exec("echo \"var trans = [];\" > " + filename);
    var filename = dirpath + '/ge.js';
    exec("echo \"var trans = [];\" > " + filename);
}

function createConfigJs(modname) {
    var dirpath = modpath + modname;
    var filename = dirpath + '/config.js';

    exec('echo \'(function (angular, undefined) {\\n'
        + '    "use strict";\\n\\n'
        + '    angular.module("' + modname + '")\\n'
        + '        .config(function ($provide, $compileProvider, $filterProvider) {\\n'
        + '        }\\n'
        + '    );\\n'
        + '})(angular);\' > ' + filename);
}

function createRoutesJs(modname) {
    var dirpath = modpath + modname;
    var filename = dirpath + '/routes.js';
    exec('echo \'(function (angular, undefined) {\\n'
        + '    "use strict";\\n\\n'
        + '    const MOD_PATH = BASE_URL + "modules/' + modname + '";\\n\\n'
        + '    angular.module("' + modname + '").registerState\\n'
        + '        .state("' + modname + '", {\\n'
        + '            breadCrumbName: "' + modname + '",\\n'
        + '            parent: "main",\\n'
        + '            url: "/' + modname + '",\\n'
        + '            templateUrl: MOD_PATH + "/components/templates/default/index.html",\\n'
        + '            controller: "' + modname + '.defaultController",\\n'
        + '            resolve: {\\n'
        + '                lazyLoadRouteAssets: ["$ocLazyLoad", function ($ocLazyLoad) {\\n'
        + '                    // you can lazy load files for an existing module\\n'
        + '                    return $ocLazyLoad.load([\\n'
        + '                        {type: "css", path: MOD_PATH + "/resources/css/module.css"},\\n'
        + '                        {type: "js", path: MOD_PATH + "/components/controllers/defaultController.js"}\\n'
        + '                    ]);\\n'
        + '                }]\\n'
        + '            }\\n'
        + '        });\\n'
        + '})(angular);\' > ' + filename);
}

function createIndexJs(modname) {
    /**
     * Controller
     */
    var dirpath = modpath + modname;
    var filename = dirpath + '/components/controllers/defaultController.js';
    exec('echo \'(function (angular, undefined) {\\n'
        + '    "use strict";\\n\\n'
        + '    angular.module("' + modname + '").controller(\\n'
        + '        "' + modname + '.defaultController",\\n'
        + '        [\\n'
        + '            "$scope", "$state", "$window",\\n'
        + '            function ($scope, $state, $window) {\\n'
        + '                //Code\\n'
        + '            }\\n'
        + '        ]\\n'
        + '    );\\n' +
        '})(angular);\' > ' + filename);

    /**
     * Tenmplate
     * @type {string}
     */
    var filename = dirpath + '/components/templates/default/index.html';
    exec('echo "' + modname + '" >  ' + filename);
}
