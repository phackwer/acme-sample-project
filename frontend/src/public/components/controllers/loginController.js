/**
 * Controller for the Login screen
 * @author Pablo Sanchez
 */
(function (angular, undefined) {
    'use strict';

    angular.module("app").controller(
        'app.loginController',
        [
            '$rootScope', '$scope', '$state', '$http', '$cookies', 'xprAcl',
            function ($rootScope, $scope, $state, $http, $cookies, xprAcl) {

                var backendUrl = config.backend + 'login/';

                $scope.formData = {
                    login: '',
                    password: ''
                }

                $scope.doEnter = function (keyEvent) {
                    if (keyEvent.which === 13) {
                        $scope.login();
                    }
                }

                var validate = function () {
                    var errors = 0;

                    if ($scope.formData.login.trim() == '') {
                        $rootScope.toastr.error('You must inform your username');
                        errors++;
                    }

                    if ($scope.formData.password.trim() == '') {
                        $rootScope.toastr.error('You must inform your password');
                        errors++;
                    }

                    return (errors == 0);
                }

                $scope.login = function () {
                    if (validate()) {
                        $http(
                            {
                                method: 'post',
                                url: backendUrl,
                                withCredentials: true,
                                headers: {
                                    'X-XSRF-TOKEN': $cookies.get('XSRF-TOKEN')
                                },
                                data: $scope.formData
                            }
                        ).then(function (data) {
                                /**
                                 * Set Current Role
                                 */
                                xprAcl.setRole("ROLE_ADMIN");

                                /**
                                 * Redirect to last accessed page
                                 */
                                var stateName =
                                    (xprAcl.getFromState() === undefined || (xprAcl.getFromState() == null)) ?
                                        'main' : xprAcl.getFromState().name;
                                var params = (xprAcl.getFromParams() === undefined) ? [] : xprAcl.getFromParams();
                                $state.go(stateName, params);
                                xprAcl.resetFromState();

                                $rootScope.toastr.success('Login successful');
                            }, function (data) {
                                $rootScope.toastr.error('Login failed: check your credentials');
                            }
                        );
                    }
                }
            }
        ]
    );
})(angular);
