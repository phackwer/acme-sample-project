/**
 * Application configuration
 * @type {{appName: string, version: string, backendURI: string, environment: string}}
 */
config = {
    // App setups
    appName: "Questions and Answers", //This you should change
    defaultStatePath: '/', //change it in case you want to redirect to a different defaultPath (#!/)
    version: "1.0.0", //This you should change
    env: "dev", //This you can change
    defaultLanguage: ['en'], //This you can change (?)
    languages: ['en', 'es', 'pt', 'fr', 'ge'], // Keep the original language at least
    dateFormat: "dd/mm/yyyy",

    //Backend definitions
    backend: null, //Please, just change it if mocking tests
    backendHost: "http://acme.dev:8000/", //This you can change
    backendPath: null, //This you can change
    backendApi: "swagger.yml", //used to create the mock server and the client
};

/**
 * Used by the controllers
 * Overwrite this when mocking or testing an specific server
 */
config.backend = config.backend ? config.backend : config.backendHost;

if (config.backendPath) {
	config.backend = config.backend + config.backendPath + '/';
}

config.backendApi = config.backendHost + config.backendApi;
