# Angular JS 1.6 Projects Skeleton

This project supports the base for Angular 1.* projects.

Please, take some time to read this: https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md
and try to follow best practices guidelines.

Code is planned to be completely developed using a modular approach, provided by a mix of
AngularJS 1.6 and SystemJS.

## Setup project in your environment

### Prerequisites

#### Git

- A good place to learn about setting up git is [here][git-setup].
- You can find documentation and download git [here][git-home].

#### PHP and Composer

- Install PHP 7 and let the CLI executable in your system path

#### Node.js and Tools / Dependencies

- Get Node.js according to your OS.
- Install the tool dependencies:

        npm install

##### Environment dependencies vs Application dependencies
 
  The environment dependency must not be confused with the application dependencies. One
  is what the server must have be install so the application can run on the specified
  environment. For example development libraries for debugging or automated tests should 
  not be available on the packed distribution on installed on the server itself, since 
  they can expose sensible information that should not be accessible by the application
  users. For instance, you should not have bower or gulp, neither the bower.json or 
  gulpfile.js on your production server. This could expose the automated tasks you
  created to a hacker to try running it remotely.
  
  The dependencies must be handled separetely on the skeleton: you have those used during
  the development time at node_modules and src/bower_components, and those that are used
  on production and that during the packing process go to the dist/libs folder minified 
  and compressed.
     
  So, how does that affect my project? Simple: you're supposed to keep your development
  tools that are used on the server side at the ./packages.json file, the ones that
  help/assist development on the client-side at the src/bower.json file in the 
  devDependencies section, and all other dependencies that are actually required for
  the application to run on the browser, should be at the src/bower.json.
  
  If you have, by any chance, node.js resources that are required keep running on the 
  server during the software execution (ex: a chat server), create them as a separated
  project or, at least, on a folder named srv, but do not place it inside de src or
  dist folders, as their content may be public accessible later.

### Create a new project using the skeleton

- Git clone the repo

        git clone --depth 1 https://github.com/phackwer/angular-admin-seed.git projectname

- Remove the .git folder from your clone.

        cd projectname && rm -rf .git
        
- Initialize the git repository and add the origin remote pointing to your github/gitlab
repository.

        git init
        git remote add origin https://remoteurlforrepository

### Start your application environment 

- To start the environment on http://localhost:8000 on development mode, type:

        npm start

- Your ambient will be setup for development, that should be done at the src folder only.
- When you finished adding features or fixing code, run the tests with:

        npm test

- If everything works fine, prepare a release versioned package with
 
        gulp release --type=[MAJOR,MINOR,FIX]
         
- Versioning will be automatic according to the type of release you have prepared, unless you force it with:

        gulp release --type=[MAJOR,MINOR,FIX] --force-version=0.0.0

### Project detailed structure

The following describes the code organization of the app, in order to help understanding
the proposal of the work realized here.

        / -project's root
        ├── ▤ gulpfile.js - gulp tasks for development and deployment (generate dist "compiled" files)
        ├── ◚ node_modules - folder for node componentes required by gulp
        ├── ◚ dist - folder where the code is optimized and prepared for production
        ├── ◚ src - folder where developers should work
        │   ├── ▤ app.js - application bootstrap
        │   ├── ▤ bower.json - packages dependencies
        │   ├── ▤ index.html - main page for the application        
        │   ├── ▤ config.js - application main configuration file
        │   ├── ▤ routes.js - application main routes (login, logout and main routes, basically)
        │   ├── ◚ libs - vendor component
        │   ├── ◚ components - folder where the application base components are located
        │   │   ├── ◚ controllers - angular controllers for the application base
        │   │   ├── ◚ directives - angular directives for the application base
        │   │   ├── ◚ models - json schema declaration for the application base (user, etc)
        │   │   ├── ◚ services - angular services for the application base
        │   │   │   ├── ◚ clients - application base services clients
        │   │   │   ├── ◚ providers - application base services providers
        │   │   │   └── ◚ utils - application utility services
        │   │   └── ◚ templates - html templates for the application base
        │   ├── ◚ resources - folder where the application base resources are located (config, routes, translations)
        │   │   ├── ◚ css - css for the application base
        │   │   ├── ◚ images - images for the application base
        │   │   ├── ◚ translations - translation files for the application base
        │   ├── ▤ modules.php - PHP that scans and prepares for loading all the modules as a json (replaced by modules.js on dist)
        │   └── ◚ modules - application modules go here and are separated in a similar organization of the application
        │       └── ◚ samplemodule - just a sample module
        │           ├── ▤ module.js - module bootstrap (register module into the application, with it's name, config, routes, services, etc)
        │           ├── ▤ config.js - application main configuration file
        │           ├── ▤ routes.js - application main routes (login, logout and main routes, basically)
        │           ├── ◚ components- folder where the application base components are located
        │           │   ├── ◚ controllers - angular controllers for the application base
        │           │   ├── ◚ directives - angular directives for the application base
        │           │   ├── ◚ models - json schema declaration for the application base (user, etc)
        │           │   ├── ◚ services - angular services for the application base
        │           │   │   ├── ◚ clients - application base services clients
        │           │   │   ├── ◚ providers - application base services providers
        │           │   │   └── ◚ utils - application utility services
        │           │   └── ◚ templates - html templates for the application base
        │           └── ◚ resources - folder where the application base resources are located (config, routes, translations)
        │               ├── ◚ css - css for the application base
        │               ├── ◚ images - images for the application base
        │               ├── ◚ translations - translation files for the application base
        └── ◚ tests - application tests that can be run agains the src or dist files

## Start to code

The folder description should be enough to figure out where every piece of your software should go, but
some explanations must be made before you begin

### 1 - Create as many modules as possible

Working in a modular way requires some planning. First try to understand the features your application will
have. Modulars usually can be reusable, but that's not a rule to define them. Avoid working on the main
folders of the application. Do that only to change the application behaviour, but never to code business 
logic. Business logic can ALWAYS be thought in a modular way. Try to group features by similarity or objective
when creating a module. For example: don't mix system administration features (manage users, manage permissions, etc)
with carrier or retailer features. They just don't match. Plus, since all systems end up having an administration 
module, it gets easier to create something reusable if it a module.
  
So, start by thinkg in a modular way.

#### Creating a Module

To create a Module, we will use the following task included on our gulpfile:

        gulp create-module modulename

### 2 - Figure out your contracts before coding (API's)

Now that you have the features planned on a modular way, you can start to write the contracts for your modules.
A frontend module usually (but not always), have a similar module on the backend, making it simple to create
a contract for both the endpoint and the client.

After having the contract figured out

### 3 - Code organization

Respect the code organization. Declare your controllers on the components/controllers folder of your module.
Declare you directives, templates, or anything you have to create that goes beyond the scope of this document,
on their respective folder, but DO NOT overlap different type of declarations on the same file, like

        angular.module('sanmple')
            .directive(...)
            .filter(...)
            .service(...)
            
It makes the code harder to maintain, because everything can be done in any order and file. Please, avoid 
making this kind of mess on the code organization. 


### 4 - Lazy load required services, directives, templates, controllers, etc, on your module routes.js

The most appropriated way to pre-load your directives and services on application bootstratp is using the config.js for 
your module and import the javascript file using the method System.import from system.js. But, you can also only load 
your directives and services on demand by using the ocLazyLoader on the routes.js file.

The ocLazyLoad and System.js are being used for lazy loading your modules. Whenever your module require
a module service or module directive, it may not have been already loaded. So, the way ocLazyLoad handles
 it is by defining a resolver for the route, that is executed when the route is accessed and before the
 controller is called. That way, the application gets lighter for the end user, but maybe a bit too verbose 
 for the programmer. 
 
 Bellow is an example of a routes.js file which has the same controller and template used for two different
 routes. In both routes, the lazy load refers to the same files. That's because the navigation can happen 
 starting by any of those two routes. But, when the second route is called, the files are already loaded
 which makes the application run smoother, faster and lighter for the end user (and network resources).
 
        "use strict";
        
        const MOD_PATH = "modules/sample";
        
        angular.module("sample").registerState
            .state("sample", {
                parent: "main",
                breadCrumbName: "Sample",
                url: "/sample/:id",
                templateUrl: "modules/sample/components/templates/default/index.html",
                controller: "sample.defaultController",
                resolve: {
                    lazyLoadRouteAssets: ["$ocLazyLoad", function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            {type: "css", path: MOD_PATH + "/resources/css/module.css"},
                            {type: "js", path: MOD_PATH + "/components/controllers/loginController.js"}
                        ]);
                    }]
                }
            })
            .state("sampleWithId", {
                parent: "main",
                breadCrumbName: "Sample",
                url: "/sample/:id",
                templateUrl: "modules/sample/components/templates/default/index.html",
                controller: "sample.defaultController",
                resolve: {
                    lazyLoadRouteAssets: ["$ocLazyLoad", function ($ocLazyLoad) {
                        // you can lazy load files for an existing module
                        return $ocLazyLoad.load([
                            {type: "css", path: MOD_PATH + "/resources/css/module.css"},
                            {type: "js", path: MOD_PATH + "/components/controllers/loginController.js"}
                        ]);
                    }]
                }
            });

### 5 - Cross domain Laravel 5.1 backend

In cases in which the backend is on a different address or even just a different port, some setups are required
to make the frontend working.

First you need to setup CORS headers on the backend. If you are using the xrs-core-module on Laravel 5.1, it's
already setup for you and you won't need to worry about it... too much. You may notice a request on app.js that
is made to the backend (/csrf) to fix that for you.
 
Second thing to notice is that Laravel will start asking you for your your credentials, and that means
the cookies it sent you on the first request. Angular should have it working automatically, according to their
documentation, but version 1.6.1 requires you to pass it explicity as an http option. Also, jQuery needs that
too for the ajax requests. So, basically, what you need to do for both frameworks (in case you are using
a jquery component like Datatables), is this:
 
 #### Angular requests (datatable example):
 
         /**
          * Angular way - Don't work with server side processing
          */
         .fromFnPromise(function () {
             return $http({
                     url: backendUrl,
                     method: 'post',
                     withCredentials: true,
                 }
             ).then(function (response) {
                     return response.data;
                 }
             );
         })
 
 #### jQuery requests:
 
        /**
          * jQuery way - work with server side paging
          */
             .newOptions()
             .withOption('serverSide', true)
             .withOption('ajax', {
                 url: backendUrl,
                 type: 'POST',
                 "data": function ( data ) {
                     data.searchData = $("#searchForm").serializeArray()
                 },
                 //Those are the options that make the Cross site voodoo possible, headers and withCredentials
                 headers: {
                     'X-XSRF-TOKEN': $cookies.get('XSRF-TOKEN')
                 },
                 xhrFields: {
                     withCredentials: true
                 },
             })

### 6 - Swagger Client for the Swagger exposed API

On the development setup of the application, you will realize that the PHP backend automatically
offers a small swagger description for the API, built on realtime. When on production environment
this file will be compiled for better performance.

To use the proposed API with Swagger, you can use the Swagger client library that will automatically
build the API on your client side.

        // initialize swagger with promise
        
        var swaggerWithPromise = new SwaggerClient({
            url: config.backendApi,
            usePromise: true
        })
        .then(function(swagger) {
            swaggerWithPromise = swagger;
            return swaggerWithPromise.pet.getPetById({petId: 7}, {responseContentType: 'application/json'});
        })
        .then(function(pet) {
            console.log('Swagger promise resolved pet', pet);
            return swaggerWithPromise.store.getOrderById({orderId: 2});
        })
        .then(function(order) {
            console.log('Swagger promise resolved order', order);
            return swaggerWithPromise.store.getOrderById();
        })
        .catch(function(error) {
            console.error('Swagger promise rejected', error);
        });

## Testing

