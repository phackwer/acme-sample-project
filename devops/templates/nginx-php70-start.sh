#!/bin/bash

# ADD USER
id -u $USERNAME &>/dev/null || useradd -u $USERID $USERNAME

# SETUP PHP-FPM AND NGINX - Required to correct permissions
echo 'security.limit_extensions = ' | tee --append /etc/php/7.0/fpm/pool.d/www.conf
sed -i -e "s/listen.owner = www-data/listen.owner = $USERNAME/g" /etc/php/7.0/fpm/pool.d/www.conf
sed -i -e "s/listen.group = www-data/listen.group = $USERNAME/g" /etc/php/7.0/fpm/pool.d/www.conf
sed -i -e "s/user = www-data/user = $USERNAME/g" /etc/php/7.0/fpm/pool.d/www.conf
sed -i -e "s/group = www-data/group = $USERNAME/g" /etc/php/7.0/fpm/pool.d/www.conf
sed -i -e "s/user www-data/user $USERNAME/g" /etc/nginx/nginx.conf

rm -f /etc/nginx/sites-enabled/*
ln -s /etc/nginx/sites-available/backend /etc/nginx/sites-enabled/backend
ln -s /etc/nginx/sites-available/frontend /etc/nginx/sites-enabled/frontend
sed -i -e "s/\$BACK_HOST_PORT/$BACK_HOST_PORT/g" /etc/nginx/sites-enabled/backend
sed -i -e "s/\$BACK_HOST_NAME/$BACK_HOST_NAME/g" /etc/nginx/sites-enabled/backend
sed -i -e "s/\$BACK_PUBLIC_FOLDER/$BACK_PUBLIC_FOLDER/g" /etc/nginx/sites-enabled/backend

set -e

service php7.0-fpm start
service nginx start

tail -f /dev/null