map $sent_http_content_type $expires {
    default                    on;
    text/html                  epoch;
    text/css                   epoch;
    application/javascript     epoch;
    ~image/                    max;
}

server {
    expires $expires;
    server_name $FRONT_HOST_NAME;
    listen $FRONT_HOST_PORT default_server;
    listen [::]:$FRONT_HOST_PORT default_server;
    root /var/www/html/frontend/$FRONT_PUBLIC_FOLDER;
    proxy_buffering off;

    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }
}