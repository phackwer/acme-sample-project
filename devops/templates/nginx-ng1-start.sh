#!/bin/bash

# ADD USER
id -u $USERNAME &>/dev/null || useradd -u $USERID $USERNAME

# SETUP NGINX - Required to correct permissions
sed -i -e "s/user www-data/user $USERNAME/g" /etc/nginx/nginx.conf

rm -f /etc/nginx/sites-enabled/*
ln -s /etc/nginx/sites-available/frontend /etc/nginx/sites-enabled/frontend

sed -i -e "s/\$FRONT_HOST_PORT/$FRONT_HOST_PORT/g" /etc/nginx/sites-enabled/frontend
sed -i -e "s/\$FRONT_HOST_NAME/$FRONT_HOST_NAME/g" /etc/nginx/sites-enabled/frontend
sed -i -e "s/\$FRONT_PUBLIC_FOLDER/$FRONT_PUBLIC_FOLDER/g" /etc/nginx/sites-enabled/frontend

set -e

service nginx start

tail -f /dev/null