server {
    server_name $BACK_HOST_NAME;
    listen $BACK_HOST_PORT default_server;
    listen [::]:$BACK_HOST_PORT default_server;
    root /var/www/html/backend/$BACK_PUBLIC_FOLDER;
    proxy_buffering off;

    index index.php;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri $uri/ =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}