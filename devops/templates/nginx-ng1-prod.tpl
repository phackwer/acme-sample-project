server {
    server_name $FRONT_HOST_NAME;
    listen $FRONT_HOST_PORT default_server;
    listen [::]:$FRONT_HOST_PORT default_server;
    root /var/www/html/frontend/$FRONT_PUBLIC_FOLDER;
    proxy_buffering off;

    index index.html;

    location / {
        try_files $uri $uri/ /index.html;
    }
}