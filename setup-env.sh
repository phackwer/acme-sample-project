#!/bin/bash

if [ ! $SUDO_USER ]; then

  echo 'Must be run with "sudo"'

else

    echo "Local Requirements"

    echo "NodeJS, PHP 7 & Composer, Docker"
    apt-get install apt-transport-https ca-certificates
    curl -fsSL https://yum.dockerproject.org/gpg | sudo apt-key add -
    add-apt-repository -y \
       "deb https://apt.dockerproject.org/repo/ \
       ubuntu-$(lsb_release -cs) \
       main"
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    add-apt-repository -y ppa:ondrej/php
    apt-get update
    apt-get install -y nodejs
    apt-get install -y --allow-unauthenticated libgeos-dev libpcre3 libpcre3-dev \
        php7.0 php7.0-zip php7.0-mcrypt php7.0-mbstring php7.0-dev php7.0-json \
        php7.0-bcmath php7.0-fpm php7.0-gd php7.0-geoip php7.0-intl \
        php7.0-xml php7.0-curl php7.0-mysql php7.0-pgsql php7.0-sqlite3
    apt-get -y install docker-engine
    wget https://getcomposer.org/installer
    php installer
    mv composer.phar /usr/bin/composer
    rm installer

    echo "Building Docker Containers"

    echo '127.0.0.1	acme.dev' | tee --append /etc/hosts

    docker network create --subnet=172.18.0.0/16 acme

    cd devops && docker build ./ -f Dockerfile-nginx-ng1 --tag acme-nginx-ng1:0.1 && cd ../
    cd devops && docker build ./ -f Dockerfile-nginx-php70 --tag acme-nginx-php70:0.1 && cd ../

    docker run \
        --name acme-nginx-ng1 \
        -p 80:80 \
        -e USERID=`id -u $SUDO_USER` \
        -e USERNAME=$SUDO_USER \
        -v `pwd`:/var/www/html \
        -e FRONT_HOST_NAME='acme.dev' \
        -e FRONT_HOST_PORT='80' \
        -e FRONT_PUBLIC_FOLDER='src' \
        --net acme \
        --ip 172.18.0.100 \
        -d acme-nginx-ng1:0.1

    docker run \
        --name acme-nginx-php70 \
        -p 8000:8000 \
        -e USERID=`id -u $SUDO_USER` \
        -e USERNAME=$SUDO_USER \
        -v `pwd`:/var/www/html \
        -e BACK_HOST_NAME='acme.dev' \
        -e BACK_HOST_PORT='8000' \
        -e BACK_PUBLIC_FOLDER='public' \
        --net acme \
        --ip 172.18.0.2 \
        -d acme-nginx-php70:0.1

    # First run is just the setup, we still need to start it after that.
    docker run \
        --name acme-mysql \
        -p 3306:3306 \
        -e USERID=`id -u $SUDO_USER` \
        -e USERNAME=$SUDO_USER \
        -e MYSQL_DATABASE='acme' \
        -e MYSQL_USER='acme' \
        -e MYSQL_PASSWORD='4cme' \
        -e MYSQL_ROOT_PASSWORD='4cme' \
        -e MYSQL_ROOT_HOST="%" \
        --net acme \
        --ip 172.18.0.3 \
        -d mysql/mysql-server:5.7

    sleep 30;

    echo "Running composer for the backend - you should have it in your path!"
    cd backend && composer install && \
    cp .env.example .env && \
    php artisan key:generate && \
    php artisan vendor:publish --provider="OwenIt\Auditing\AuditingServiceProvider" && \
    php artisan migrate && php artisan db:seed && cd ../

    echo "Running npm for the frontend - you should have it in your path!"
    cd frontend
    npm install
    cd src
    bower install
    cd ../../

    echo "Fixing permissions"
    chown -R $SUDO_USER: *  && chown -R $SUDO_USER: /home/$SUDO_USER/*

    echo "*******************************************************************************************************************"
    echo ""
    echo "You can now access your frontend at http://acme.dev."
    echo "Backend is running on the same address, port 8000."
    echo "PostgreSQL is running on the same address, port 54321."
    echo ""
    echo "*******************************************************************************************************************"
    echo ""
    echo ""

fi;
