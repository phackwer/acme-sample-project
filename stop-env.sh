#!/bin/bash

if [ ! $SUDO_USER ]; then

  echo 'Must be run with "sudo"'

else

    echo "Fixing permissions"
    chown -R $SUDO_USER: *  && chown -R $SUDO_USER: /home/$SUDO_USER/*

    docker stop acme-mysql acme-nginx-php70 acme-nginx-ng1

    echo "*******************************************************************************************************************"
    echo ""
    echo "Docker stop. Ports 80,  8000 and 3306 are freed."
    echo ""
    echo "*******************************************************************************************************************"
    echo ""
    echo ""

fi;