SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS user_answer;
DROP TABLE IF EXISTS answer;
DROP TABLE IF EXISTS question;
DROP TABLE IF EXISTS form;
DROP TABLE IF EXISTS users;




/* Create Tables */

CREATE TABLE answer
(
	id int NOT NULL AUTO_INCREMENT,
	question_id int NOT NULL,
	valid_value varchar(256) NOT NULL,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
	updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted_at datetime,
	PRIMARY KEY (id)
);


CREATE TABLE form
(
	id int NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	name varchar(256) NOT NULL,
	description text,
	introduction text,
	start_publish datetime,
	end_publish datetime,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
	updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted_at datetime,
	PRIMARY KEY (id),
	UNIQUE (name)
);


CREATE TABLE question
(
	id int NOT NULL AUTO_INCREMENT,
	form_id int NOT NULL,
	description text NOT NULL,
	mandatory boolean DEFAULT false NOT NULL,
	type enum('number','text', 'date', 'radio', 'dropdown') NOT NULL,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
	updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted_at datetime,
	PRIMARY KEY (id)
);


CREATE TABLE users
(
	id int NOT NULL AUTO_INCREMENT,
	name varchar(256),
	email varchar(256) NOT NULL,
	password varchar(60),
	admin boolean DEFAULT false NOT NULL,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
	updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted_at datetime,
	PRIMARY KEY (id),
	UNIQUE (email)
);


CREATE TABLE user_answer
(
	user_id int NOT NULL,
	question_id int NOT NULL,
	answer_id int NOT NULL,
	text_answer text,
	date_answer date,
	number_answer float,
	created_at datetime DEFAULT CURRENT_TIMESTAMP,
	updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	deleted_at datetime
);



/* Create Foreign Keys */

ALTER TABLE user_answer
	ADD FOREIGN KEY (answer_id)
	REFERENCES answer (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE question
	ADD FOREIGN KEY (form_id)
	REFERENCES form (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE answer
	ADD FOREIGN KEY (question_id)
	REFERENCES question (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE user_answer
	ADD FOREIGN KEY (question_id)
	REFERENCES question (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE form
	ADD FOREIGN KEY (user_id)
	REFERENCES users (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE user_answer
	ADD FOREIGN KEY (user_id)
	REFERENCES users (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



