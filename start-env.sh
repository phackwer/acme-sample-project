#!/bin/bash

if [ ! $SUDO_USER ]; then

  echo 'Must be run with "sudo"'

else

    echo "Fixing permissions"

    chown -R $SUDO_USER: *  && chown -R $SUDO_USER: /home/$SUDO_USER/*

    # docker start geofinder-prod-pg95gis22 geofinder-prod-nginx-php70 geofinder-prod-nginx-ng1
    docker start acme-mysql acme-nginx-php70 acme-nginx-ng1

    echo "*******************************************************************************************************************"
    echo ""
    echo "You can now access your frontend at http://acme.dev"
    echo "Backend is running on the same address, port 8000."
    echo "MySQL is running on the same address, port 3306."
    echo ""
    echo "*******************************************************************************************************************"
    echo ""
    echo ""

fi;