# Q&A Survey Test

The Q&A Survey Test application was created having DevOps in mind, plus the concept of having completely separated
 backend and frontend for the application, making switching technologies or reuse of elements from it a bit
 more friendly for the developer.
 
To be able to work with it, your environment must meet one of the following requirements:

- Docker (Scenario 1 only)
- NodeJS
- PHP 7
- MySQL 5.7 (Scenario 2 only)

## Fastest way:

    sudo ./setup-env.sh

After setup, your docker container will be already running. If you need to start it again later, than type:
 
    sudo ./start-env.sh
     
Now open http://acme.dev
 
## Scenario 1 - Using Docker

A Dockerfile is provided for this project, which you can find it inside the "devops" folder. This Dockerfile
brings up a Ubuntu 16.04 LTS instance with NGinx, PHP 7.0, and MySQL pre-setup for the developer
and production environment. If you choose this option, all you need to have is Docker installed and running
on your computer (check Docker documentation for details on how to install it on your operating system).

Check the README.md on "devops" for further detailing

## Scenario 2 - Running locally

If you prefer not to use Docker for whatever reason you have, you will need to setup a local installation of
MySQL. (check MySQL documentation for details on how to install it on your operating system).

Check the README.md files on both frontend and backend for specific details on how they are setup.

