<?php

namespace Core\Model\Db;

use XCore\Model;
use OwenIt\Auditing\AuditingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use AuditingTrait;
    use SoftDeletes;

    protected $table = "question";

    protected $auditableTypes = ['created', 'saved', 'deleted'];

    protected $dates = ['deleted_at'];

    public function form()
    {
        return $this->belongsTo('\Core\Model\Db\Form');
    }

    public function answers()
    {
        return $this->hasMany('\Core\Model\Db\Answer');
    }

    public function usersanswers()
    {
        return $this->hasMany('\Core\Model\Db\UserAnswer');
    }
}