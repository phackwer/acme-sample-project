<?php

namespace Core\Model\Db;

use XCore\Model;
use OwenIt\Auditing\AuditingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAnswer extends Model
{
    use AuditingTrait;
    use SoftDeletes;

    protected $table = "user_answer";

    protected $auditableTypes = ['created', 'saved', 'deleted'];

    protected $dates = ['deleted_at'];

    public function question()
    {
        return $this->belongsTo('\Core\Model\Db\Question');
    }

    public function answer()
    {
        return $this->belongsTo('\Core\Model\Db\Answer');
    }

    public function user()
    {
        return $this->belongsTo('\Core\Model\Db\User');
    }
}