<?php

namespace Core\Model\Db;

use XCore\Model;
use OwenIt\Auditing\AuditingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use AuditingTrait;
    use SoftDeletes;

    protected $table = "answer";

    protected $auditableTypes = ['created', 'saved', 'deleted'];

    protected $dates = ['deleted_at'];

    public function question()
    {
        return $this->belongsTo('\Core\Model\Db\Question');
    }

    public function usersanswers()
    {
        return $this->hasMany('\Core\Model\Db\UserAnswer');
    }
}