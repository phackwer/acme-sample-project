<?php

namespace Core\Model\Db;

use XCore\Model;
use OwenIt\Auditing\AuditingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form extends Model
{
    use AuditingTrait;
    use SoftDeletes;

    protected $table = "form";

    protected $auditableTypes = ['created', 'saved', 'deleted'];

    protected $dates = ['deleted_at'];

    public function author()
    {
        return $this->belongsTo('\Core\Model\Db\User');
    }

    public function questions()
    {
        return $this->hasMany('\Core\Model\Db\Question');
    }
}