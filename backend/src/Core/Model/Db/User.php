<?php

namespace Core\Model\Db;

use XCore\Model;
use OwenIt\Auditing\AuditingTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use AuditingTrait;
    use SoftDeletes;

    protected $table = "users";

    protected $auditableTypes = ['created', 'saved', 'deleted'];

    protected $dates = ['deleted_at'];

    public function answers()
    {
        return $this->hasMany('\Core\Model\Db\UserAnswer');
    }
}