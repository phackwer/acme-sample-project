<?php

namespace Core\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\UnauthorizedException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Core\Model\Db\User;

class LoginController extends Controller
{
    /**
     * $request Request
     *
     * @var string
     */
    protected $request = null;

    /**
     * Create a new business provider instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application $app
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function login()
    {
        $user = User::where([
            'email' => $this->request->get('login'),
            'password' => md5($this->request->get('password')),
        ])->first();

        if ($user) {
            session(['user' => $user]);
            return response()->json(['status' => 'OK']);
        } else {
            throw new UnauthorizedException('User could not be authenticated with this credentials.');
        }
    }

    public function logout()
    {
        session(['user' => null]);
    }
}