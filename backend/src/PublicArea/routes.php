<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
 */
Route::group([
    'prefix'    => '/publicarea',
    'namespace' => 'PublicArea\Controller'
], function () {
    Route::get('/', function () {
        return response()->json(['status' => 'OK']);
    });
});