<?php

namespace Administration\Controller;

use XCore\Controller\RestApiController;

class FormController extends RestApiController
{
    /**
     * Default route to show that the controller is up and running on desired path
     * @Route("/", methods={"GET"} )
     */
    public function index($data = null)
    {
        return response()->json(
            array('msg' => 'OK')
        );
    }
}