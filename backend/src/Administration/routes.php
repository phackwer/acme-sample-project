<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
 */
Route::group([
    'prefix'    => '/administration',
    'namespace' => 'Administration\Controller'
], function () {
    Route::get('/', function () {
        return response()->json(['status' => 'OK']);
    });

    Route::get('/form', 'FormController@search');
    Route::post('/form', 'FormController@search');
    Route::get('/form/{id}', 'FormController@show');
    Route::put('/form', 'FormController@put');
    Route::delete('/form/{id}', 'FormController@delete');

});