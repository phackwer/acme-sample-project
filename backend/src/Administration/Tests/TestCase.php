<?php

/**
 * REMEMBER TO CHANGE THE BOOTSTRAP PATH IF YOU CONVERT
 * THE MODULE INTO A VENDOR PACKAGE
 */

namespace Administration\Tests;

use \Illuminate\Foundation\Testing\TestCase as TestBase;
use \Illuminate\Contracts\Console\Kernel;

class TestCase extends TestBase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        /**
         * REMEMBER TO CHANGE THE BOOTSTRAP PATH IF YOU CONVERT
         * THE MODULE INTO A VENDOR PACKAGE
         * $app = require __DIR__.'/../../../../bootstrap/app.php';
         */
        $app = require __DIR__.'/../../../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
