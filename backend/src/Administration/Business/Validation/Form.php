<?php

/**
 * Copyright 2007 Xpreso Software Ltd
 * @Author Pablo Santiago Sanchez <psanchez@xpreso.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Administration\Business\Validation;

use XCore\Business\Validation\ModelValidatorInterface;
use XCore\Model;

class Form implements ModelValidatorInterface
{
    public function saveValidation(Model $form)
    {
        $errors = [];
        /**
         * Require name
         */
        if (trim($form->name) == "") {
            $errors[] = 'Invalid form name';
        }

        if ($form->start_publish &&
            $form->start_publish < date('Y-m-d')
        ) {
            $errors[] = 'Start publishing date cannot be in the past';
        }

        /**
         * Compare start publishing and end dates
         */
        if ($form->end_publish &&
            $form->end_publish <= $form->start_publish
        ) {
            $errors[] = 'Invalid publishing period';
        }

        /**
         * Require at least 1 question
         */
        if (count($form->questions) == 0) {
            $errors[] = 'You need to add at least one question';
        }

        /**
         * Require filled answers for dropbox and radio
         */
        foreach ($form->questions as $question) {
            /**
             * It's mandatory to inform a description
             */
            if (trim($question->description) == '') {
                $errors[] = 'You need to inform descriptions for all your questions';
            }

            /**
             * Can't change type of a questions if it has user answers
             * @TODO
             */

            /**
             * Minimum number of answers
             */
            if ($question->type == 'radio' || $question->type == 'dropbox') {
                if (count($question->answers) < 2) {
                    $errors[] = 'Questions of type '.$question->type.' requires at least 2 answers';
                }
            }

            /**
             * Can't change label of an answer if it has user answers
             * @TODO
             */

            /**
             * Can't change mandatory field of question if already published
             * @TODO
             */

        }
    }

    public function deleteValidation(Model $form)
    {
    }
}