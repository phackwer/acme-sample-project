<?php

namespace Administration\Business;

use Core\Model\Db\Answer;
use Core\Model\Db\Question;
use XCore\Business\Model;

class Form extends Model
{
    protected $modelClass = '\\Core\\Model\\Db\\Form';

    /**
     * Fill form instance with submitted data
     *
     * @param  [type] $model [description]
     * @param  [type] $data  [description]
     * @return [type]        [description]
     */
    public function fillModel($form, $data)
    {
        unset($data['start_date']);
        unset($data['end_date']);
        $form->deleted = $data['deleted'];
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ($key != 'questions') {
                    $form->$key = $value;
                } else {
                    $questions = [];
                    foreach ($value as $q => $question) {
                        $questions[$q] = $question['id']
                            ? Question::where(['id' => $question['id']])->first()
                            : new Question();
                        $answers = [];
                        foreach ($question as $qkey => $qvalue) {
                            if ($qkey != 'answers') {
                                $questions[$q]->$qkey = $qvalue;
                            } else {
                                foreach ($qvalue as $akey => $avalue) {
                                    $answers[$akey] = $avalue['id']
                                        ? Answer::where(['id' => $avalue['id']])->first()
                                        : new Answer();
                                    $answers[$akey]->valid_value = $avalue['valid_value'];
                                }
                            }
                        }
                        $questions[$q]->answers = $answers;
                    }
                    $form->questions = $questions;
                }
            }
            $form->user_id = session('user')->id;
        }
    }

    public function getBy($attribute, $value, $model = null)
    {
        $repo = $this->getRepository();
        $model = $repo->getBy($attribute, $value, $model)[0];
        $questions = $model->questions;
        foreach ($questions as $question) {
            $question->answers;
        }
        return $model;
    }
}
