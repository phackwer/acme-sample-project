<?php

namespace Administration\Repository;

use Core\Model\Db\Answer;
use Core\Model\Db\Question;
use XCore\Repository;
use Illuminate\Support\Facades\DB;

class Form extends Repository
{
    protected $modelClass = '\\Core\\Model\\Db\\Form';

    /**
     * Persiste o modelo no banco de dados, de forma transacional
     *
     * @param  \Multi\Core\Model $form [description]
     * @param  [type] $data[description]
     * @return [type]        [description]
     * @throws \Exception
     */
    public function persistModel($form)
    {
        try {
            DB::beginTransaction();
            /**
             * Delete all the deleted questions or answers
             */
            foreach ($form->deleted['questions'] as $id) {
                if ($question = Question::where(['id' => $id])->first()) {
                    $question->delete();
                }
            }

            foreach ($form->deleted['answers'] as $id) {
                if ($answer = Answer::where(['id' => $id])->first()) {
                    $answer->delete();
                }
            }

            unset($form->deleted);

            /**
             * Clear form model to avoid error on saving
             */
            $questions = $form->questions;
            $form->questions = null;
            unset($form->questions);
            $form->save();
            $form->questions = $questions;

            foreach ($questions as $question) {
                /**
                 * Clear question model to avoid error on saving
                 */
                $answers = $question->answers;
                $question->answers = null;
                unset($question->answers);

                $question->form_id = $form->id;
                $question->save();
                $question->answers = $answers;

                /**
                 * Process answers
                 */
                foreach ($answers as $answer) {
                    $answer->question_id = $question->id;
                    $answer->save();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return $form;
    }
}