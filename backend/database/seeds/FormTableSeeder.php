<?php

use Illuminate\Database\Seeder;

class FormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('form')->insert([
//            'id' => 1,
//            'user_id' => 1,
//            'name' => 'Form #' . 1,
//            'description' => 'This form is an autoseed and serve as base for development purposes only.',
//            'introduction' => 'This is a sample for a introduction text for this form. It\'s intended to present the ' .
//                'form to the public, and just that.',
//            'start_publish' => DateTime::createFromFormat('Y-m-d H:m:s', '1978-08-18 01:00:00'),
//            'end_publish' => null,
//        ]);
        for ($i = 1; $i < 70; $i++) {
            DB::table('form')->insert([
                'id' => $i,
                'user_id' => 1,
                'name' => 'Form #'.$i,
                'description' => 'This form is an autoseed and serve as base for development purposes only.',
                'introduction' => 'This is a sample for a introduction text for this form. It\'s intended to present the ' .
                    'form to the public, and just that.',
                'start_publish' => DateTime::createFromFormat('Y-i-d H:m:s', '1978-08-18 01:00:00'),
                'end_publish' => null,
            ]);
        }
    }
}
