<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question')->insert([
            'id' => 1,
            'form_id' => 1,
            'description' => 'Description of Question #1',
            'mandatory' => true,
            'type' => 'dropdown',
        ]);

        DB::table('question')->insert([
            'id' => 2,
            'form_id' => 1,
            'description' => 'Description of Question #2',
            'mandatory' => false,
            'type' => 'text',
        ]);
    }
}
