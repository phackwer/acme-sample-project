<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 5; $i++) {
            DB::table('answer')->insert([
                'id' => $i,
                'question_id' => 1,
                'valid_value' => 'Answer #'.$i,
            ]);
        }
    }
}
