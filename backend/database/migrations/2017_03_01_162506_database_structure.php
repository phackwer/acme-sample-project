<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatabaseStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dsp = DIRECTORY_SEPARATOR;
        $sql = file_get_contents(base_path().$dsp.'..'.$dsp.'docs'.$dsp.'database_model'.$dsp.'database.sql');

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
